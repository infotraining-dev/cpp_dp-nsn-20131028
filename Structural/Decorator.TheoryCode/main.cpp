#include "decorator.hpp"

int main()
{
	 // Create ConcreteComponent and two Decorators
     Component* c = new ConcreteDecoratorB(
                 new ConcreteDecoratorA(new ConcreteDecoratorA(new ConcreteComponent)));

     c->operation();

	 std::cout << std::endl;

     delete c;
}
