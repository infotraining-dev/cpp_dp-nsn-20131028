#include "coffeehell.hpp"
#include <memory>


class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename T>
    CoffeeBuilder& create_base()
    {
        coffee_.reset(new T());
        return *this;
    }

    template <typename T>
    CoffeeBuilder& add()
    {
        Coffee* temp = coffee_.release();
        coffee_.reset(new T(temp));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
    Coffee* cf = new Whipped(new Whisky(new Whisky(new Whisky(new Whisky(new Espresso())))));

	std::cout << "Description: " << cf->get_description() << "; Price: " << cf->get_total_price() << std::endl;
	cf->prepare();

	delete cf;

    std::cout << "\n\n";

    CoffeeBuilder cb;
    cb.create_base<Espresso>().add<Whisky>().add<Whipped>();

    auto drink = cb.get_coffee();

    std::cout << "Description: " << drink->get_description() << "; Price: "
              << drink->get_total_price() << std::endl;

    drink->prepare();
}
