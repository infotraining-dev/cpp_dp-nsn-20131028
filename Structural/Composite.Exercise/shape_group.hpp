#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <list>
#include <memory>
#include <functional>

namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::list<std::shared_ptr<Shape>> shapes_;
public:
    ShapeGroup()
    {
    }

    ShapeGroup(const ShapeGroup& source)
    {
        for(auto shape : source.shapes_)
        {
            shapes_.push_back(std::shared_ptr<Shape>(shape->clone()));
        }
    }

    void swap(ShapeGroup& sg)
    {
        shapes_.swap(sg.shapes_);
    }

    ShapeGroup& operator=(const ShapeGroup& source)
    {
        ShapeGroup temp(source);
        swap(temp);

        return *this;
    }

    void add(std::shared_ptr<Shape> shape)
    {
        shapes_.push_back(shape);
    }

    void draw() const
    {
        std::for_each(shapes_.begin(), shapes_.end(), std::mem_fn(&Shape::draw));
    }

    void move(int dx, int dy)
    {
        std::for_each(shapes_.begin(), shapes_.end(),
                      std::bind(&Shape::move, std::placeholders::_1, dx, dy));
    }

    ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream &in)
    {
        int count;

        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_identifier;
            in >> type_identifier;

            std::shared_ptr<Shape> shape(ShapeFactory::instance().create(type_identifier));
            shape->read(in);
            add(shape);
        }
    }

    void write(std::ostream &out)
    {
        std::for_each(shapes_.begin(), shapes_.end(),
                      std::bind(&Shape::write, std::placeholders::_1, std::ref(out)));
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
