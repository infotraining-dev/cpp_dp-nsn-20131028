#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>

#include "factory.hpp"

using namespace std;

class Client
{
private:
	vector<Product*> products_;
	Creator* creator_;
public:
	Client(Creator* creator) : creator_(creator)
	{
	}

	Client(const Client&);
	Client& operator=(const Client&);

	~Client()
	{
		vector<Product*>::const_iterator it = products_.begin();
		for( ; it != products_.end(); ++it)
			delete *it;
	}

	void init(size_t size)
	{
		for(size_t i = 0; i < size; ++i)
			products_.push_back(creator_->factory_method());
	}

	void use()
	{
		cout << "Content of client:\n";
		vector<Product*>::const_iterator it = products_.begin();
		for( ; it != products_.end(); ++it)
			cout << (*it)->description() << endl;
	}
};

class ProductC : public Product
{
public:
    std::string description() const
    {
        return std::string("ProductC");
    }
};

class CreatorC : public Creator
{
public:
    Product* factory_method()
    {
        return new ProductC();
    }
};

int main()
{
	std::vector<Creator*> creators;
	creators.push_back(new ConcreteCreatorA());
	creators.push_back(new ConcreteCreatorB());
    creators.push_back(new CreatorC());

    Client client(creators[2]);
	client.init(10);
	client.use();

	// sprzątanie
	for(vector<Creator*>::iterator it = creators.begin(); it != creators.end(); ++it)
		delete *it;
}
