#include <iostream>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
protected:
    virtual Engine* do_clone() const = 0;
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    Engine* clone() const
    {
        Engine* cloned_engine = do_clone();

        assert(typeid(*cloned_engine) == typeid(*this));

        return cloned_engine;
    }

    ~Engine() {}
};

class Diesel : public Engine
{
public:
    virtual void start() { cout << "Start Diesel\n"; }
    virtual void stop() { cout << "Stop Diesel\n"; }
protected:
    virtual Diesel* do_clone() const
    {
        return new Diesel(*this);
    }
};

class TDI : public Diesel
{
public:
    virtual void start() { cout << "Start TDI\n"; }
    virtual void stop() { cout << "Stop TDI\n"; }
protected:
    virtual TDI* do_clone() const
    {
        return new TDI(*this);
    }
};


class Hybrid : public Engine
{
public:
    virtual void start() { cout << "Start Hybrid\n"; }
    virtual void stop() { cout << "Stop Hybrid\n"; }
protected:
    virtual Hybrid* do_clone() const
    {
        return new Hybrid(*this);
    }
};

class Car
{
    Engine* engine_;
public:
    Car(Engine* engine) : engine_(engine)
    {
    }

    Car(const Car& source) : engine_(source.engine_->clone())
    {
    }

    ~Car()
    {
        delete engine_;
    }

    void drive(int distance)
    {
        engine_->start();
        cout << "Driving: " << distance << " kms" << endl;
        engine_->stop();
    }
};

int main()
{
    Car c1(new TDI());

    c1.drive(100);

    cout << "\n\n";

    Car copy_of_c1 = c1;

    copy_of_c1.drive(300);

    return 0;
}

