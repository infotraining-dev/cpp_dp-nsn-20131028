#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>

class AccountState
{
public:
    AccountState(double balance) : balance_(balance)
    {
    }

    double balance() const
    {
        return balance_;
    }

    virtual std::shared_ptr<AccountState> withdraw(double amount) = 0;

    virtual std::shared_ptr<AccountState> deposit(double amount)
    {
        balance_ += amount;

        return get_next_state();
    }

    virtual std::shared_ptr<AccountState> pay_interest() = 0;

    virtual void print_status() const = 0;

protected:
    virtual std::shared_ptr<AccountState> get_next_state();

    double balance_;
};

class OverdraftState : public AccountState
{
    static double OVERDRAFT_INTEREST_RATE;
public:
    OverdraftState(double balance) : AccountState(balance)
    {
    }

    virtual std::shared_ptr<AccountState> withdraw(double amount)
    {
        std::cout << "Brak srodkow na koncie!" << std::endl;

        return get_next_state();
    }

    virtual std::shared_ptr<AccountState> pay_interest()
    {
        balance_ += balance_ * OVERDRAFT_INTEREST_RATE;

        return get_next_state();
    }

    virtual void print_status() const
    {
        std::cout << "Overdraft State ";
    }
};

class NormalState : public AccountState
{
    static double NORMAL_INTEREST_RATE;
public:
    NormalState(double balance) : AccountState(balance)
    {
    }

    virtual std::shared_ptr<AccountState> withdraw(double amount)
    {
        balance_ -= amount;

        return get_next_state();
    }

    virtual std::shared_ptr<AccountState> pay_interest()
    {
        balance_ += balance_ * NORMAL_INTEREST_RATE;

        return get_next_state();
    }

    virtual void print_status() const
    {
        std::cout << "Normal State ";
    }
};


class BankAccount
{
    int id_;
    std::shared_ptr<AccountState> state_;
public:
    BankAccount(int id) : id_(id), state_(std::shared_ptr<AccountState>(new NormalState(0.0))) {}

    void withdraw(double amount)
    {
        assert(amount > 0);

        state_ = state_->withdraw(amount);
    }

    void deposit(double amount)
    {
        assert(amount > 0);

        state_ = state_->deposit(amount);
    }

    void pay_interest()
    {
        state_ = state_->pay_interest();
    }

    void print_status() const
    {
        std::cout << "BankAccount #" << id_ << "; State: ";

        state_->print_status();

        std::cout << "Balance = " << state_->balance() << std::endl;
    }

    double balance() const
    {
        return state_->balance();
    }

    int id() const
    {
        return id_;
    }
};

namespace Legacy
{

enum AccountState { OVERDRAFT, NORMAL };

class BankAccount
{
    int id_;
    double balance_;
    AccountState state_;
protected:
    void update_account_state()
    {
        if (balance_ < 0)
            state_ = OVERDRAFT;
        else
            state_ = NORMAL;
    }

    void set_balance(double amount)
    {
        balance_ = amount;
    }
public:
    BankAccount(int id) : id_(id), balance_(0.0), state_(NORMAL) {}

    void withdraw(double amount)
    {
        assert(amount > 0);

        if (state_ == OVERDRAFT)
        {
            std::cout << "Brak srodkow na koncie #" << id_ <<  std::endl;
        }
        else // state_ == NORMAL
        {
            balance_ -= amount;

            update_account_state();
        }
    }

    void deposit(double amount)
    {
        assert(amount > 0);

        balance_ += amount;

        update_account_state();
    }

    void pay_interest()
    {
        if (state_ == OVERDRAFT)
            balance_ += balance_ * 0.15;
        else // state_ == NORMAL
            balance_ += balance_ * 0.05;
    }

    void print_status() const
    {
        std::cout << "BankAccount #" << id_ << "; State: ";

        if (state_ == OVERDRAFT)
            std::cout << "Overdraft; ";
        else // state_ == NORMAL
            std::cout << "Normal; ";

        std::cout << "Balance: " << balance_ << std::endl;
    }

    double balance() const
    {
        return balance_;
    }

    int id() const
    {
        return id_;
    }
};

}
#endif
