#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>

class Product
{
public:
    virtual void run() = 0;
    virtual ~Product() {}
};

class ProductA : public Product
{
public:
    virtual void run()
    {
        std::cout << "ProductA is running" << std::endl;
    }
};

class ProductB : public Product
{
public:
    virtual void run()
    {
        std::cout << "ProductB is running" << std::endl;
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;
    virtual bool is_valid() = 0;
    virtual Product* create_product()
    {
        return new ProductA;
    }

public:
    void template_method() // metoda szablona
	{
		primitive_operation_1();

        if (is_valid())
            primitive_operation_2();

        Product* p = create_product(); // metoda wytworcza
        p->run();
        delete p;

        std::cout << std::endl;
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}

    bool is_valid()
    {
        return false;
    }
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    bool is_valid()
    {
        return true;
    }

    Product* create_product()
    {
        return new ProductB();
    }
};

#endif /*TEMPLATE_METHOD_HPP_*/
