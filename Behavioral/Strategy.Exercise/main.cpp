#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <memory>
#include <functional>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

class Results
{
public:
	typedef std::list<StatResult>::const_iterator ResultIterator;

	void add_result(const StatResult& result)
	{
		results_.push_back(result);
	}

	ResultIterator begin() const
	{
		return results_.begin();
	}

	ResultIterator end() const
	{
		return results_.end();
	}

	void clear()
	{
		results_.clear();
	}

private:	
	std::list<StatResult> results_;
};

//enum StatisticsType
//{
//	AVG, MINMAX, SUM
//};

class Statistics
{
public:
    virtual void calculate(const std::vector<double>& data, Results& results) = 0;
    ~Statistics() {}
};

class Avg : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.add_result(result);
    }
};

class MinMax : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));

        results.add_result(StatResult("MIN", min));
        results.add_result(StatResult("MAX", max));
    }
};

class Sum : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.add_result(StatResult("SUM", sum));
    }
};

class StatGroup : public Statistics
{
    std::list<std::shared_ptr<Statistics>> stats_;
public:
    void add(std::shared_ptr<Statistics> stat)
    {
        stats_.push_back(stat);
    }

    void calculate(const std::vector<double>& data, Results& results)
    {
        std::for_each(stats_.begin(), stats_.end(),
                      std::bind(&Statistics::calculate, std::placeholders::_1,
                                std::cref(data), std::ref(results)));
    }
};

class DataAnalyzer
{
    std::shared_ptr<Statistics> statistics_;
	std::vector<double> data_;
public:
	DataAnalyzer()
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

	void save_data(const std::string& file_name) const
	{ 
		std::ofstream fout(file_name.c_str());
		if (!fout)
			throw std::runtime_error("File not opened");

		for(std::vector<double>::const_iterator it = data_.begin(); it != data_.end(); ++it)
			fout << (*it) << std::endl;
	}

    void set_statistics(std::shared_ptr<Statistics> statistics)
	{
        statistics_ = statistics;
	}

	void calculate(Results& results)
    {
        statistics_->calculate(data_, results);
	}
};

void print_results(const Results& results)
{
	for(Results::ResultIterator it = results.begin(); it != results.end(); ++it)
		std::cout << it->description << " = " << it->value << std::endl;
}

int main()
{
	Results results;

	DataAnalyzer da;
	da.load_data("data.dat");

    std::shared_ptr<Statistics> avg(new Avg());
    std::shared_ptr<Statistics> minmax(new MinMax());
    std::shared_ptr<Statistics> sum(new Sum());

    std::shared_ptr<StatGroup> sg(new StatGroup());
    sg->add(avg);
    sg->add(minmax);
    sg->add(sum);

    da.set_statistics(sg);
	da.calculate(results);

    print_results(results);

	std::cout << "\n\n";

	results.clear();
	da.load_data("new_data.dat");
	da.calculate(results);

	print_results(results);
}
